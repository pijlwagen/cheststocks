package com.michel3951.cheststocks.Support;

import org.bukkit.Bukkit;

public class Logger {

    public static void info(String message) {
        System.out.println("[ChestStocks] " + message);
    }

    public static void error(String message) {
        Bukkit.getConsoleSender().sendMessage(Chat.colored("[ChestStocks] &4" + message));
    }
}
