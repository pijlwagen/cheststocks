package com.michel3951.cheststocks.Events;

import com.Acrobot.ChestShop.Events.TransactionEvent;
import com.michel3951.cheststocks.Support.Logger;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.Instant;

public class OnChestShopTransaction implements Listener {

    protected Connection conn;

    public OnChestShopTransaction(Connection conn) {
        this.conn = conn;
    }

    @EventHandler
    public void chestShopTransaction(TransactionEvent e) {
        String type = e.getTransactionType().toString();
        ItemStack[] i = e.getStock();
        Double price = Double.valueOf(e.getExactPrice().toString());
        Integer amount = 0;
        String name = null;

        for (ItemStack item : i) {
            if (name == null) {
                name = item.getType().toString();

                if (item.getType().toString() == "ENCHANTED_BOOK") {
                    EnchantmentStorageMeta meta = (EnchantmentStorageMeta) item.getItemMeta();
                    if (meta.hasStoredEnchants()) {
                        for (Enchantment ec : meta.getStoredEnchants().keySet()) {
                            name += "_" + ec.getKey().getKey().toUpperCase();
                        }
                    }
                } else if (item.getType().toString() == "POTION") {
                    PotionMeta meta = (PotionMeta) item.getItemMeta();
                    PotionData data = meta.getBasePotionData();

                    name += "_" + data.getType().toString();
                }
            }

            amount += item.getAmount();
        }

        saveTransaction(type, name, price, amount);
    }

    private void saveTransaction(String type, String i, Double p, Integer am) {
        try {
            String query = "INSERT INTO `transactions` (`type`, `item`, `price`, `amount`, `timestamp`) VALUES (?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, type);
            stmt.setString(2, i);
            stmt.setDouble(3, p);
            stmt.setInt(4, am);
            stmt.setString(5, String.valueOf(Instant.now().toEpochMilli()));
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.error(ex.toString());
        }
    }
}
